﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 4.1, 5, 2.45, 7.4, 9, 1.8, 3.4, -2.2, 3.3, 15.3, 0.9, 11.1 };
            NumberSequence numberSequence = new NumberSequence(array);

            numberSequence.SetSortStrategy(new SequentialSort());
            numberSequence.Sort();
            Console.WriteLine("Sequential Sort:\n" + numberSequence.ToString());

            numberSequence.SetSortStrategy(new BubbleSort());
            numberSequence.Sort();
            Console.WriteLine("Bubble Sort:\n" + numberSequence.ToString());

            numberSequence.SetSortStrategy(new CombSort());
            numberSequence.Sort();
            Console.WriteLine("Comb Sort:\n" + numberSequence.ToString());
        }
    }
}
