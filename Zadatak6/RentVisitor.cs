﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class RentVisitor : IVisitor
    {
        private const double itemTax = 0.1;

        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
                return double.NaN;
            else
                return DVDItem.Price * itemTax;
        }

        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * itemTax;
        }

        public double Visit(Book book)
        {
            return book.Price * itemTax;
        }
    }
}
