﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            IItem DVDItem = new DVD("Avangers", DVDType.MOVIE, 59.99);            
            IItem VHSItem = new VHS("Rambo", 29.99);
            IItem book = new Book("Harry Potter", 34.99);

            Console.WriteLine(DVDItem.ToString());
            Console.WriteLine(VHSItem.ToString());
            Console.WriteLine(book.ToString());

            RentVisitor visitor = new RentVisitor();

            Console.WriteLine("\n" + DVDItem.Accept(visitor));
            Console.WriteLine(VHSItem.Accept(visitor));
            Console.WriteLine(book.Accept(visitor));
        }
    }
}
