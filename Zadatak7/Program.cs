﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            Cart cart = new Cart();
            cart.AddItem(new DVD("Avangers", DVDType.MOVIE, 59.99));
            cart.AddItem(new VHS("Rambo", 29.99));
            cart.AddItem(new Book("Harry Potter", 34.99));

            RentVisitor visitor = new RentVisitor();

            Console.WriteLine(cart.Accept(visitor));
            Console.WriteLine(cart.Accept(new BuyVisitor()));
        }
    }
}
