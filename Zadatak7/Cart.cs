﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Cart : IItem
    {
        private List<IItem> items;

        public Cart()
        {
            this.items = new List<IItem>();
        }

        public void AddItem(IItem item)
        {
            this.items.Add(item);
        }

        public void RemoveItem(IItem item)
        {
            this.items.Remove(item);
        }

        public double Accept(IVisitor visitor)
        {
            double totalPrice=0;
            foreach(IItem item in this.items)
            {
                totalPrice += item.Accept(visitor);
            }
            return totalPrice;
        }
    }
}
