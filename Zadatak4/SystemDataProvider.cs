﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;

        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }

        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            float loadChange = currentLoad - this.previousCPULoad;
            if (loadChange < 0) { loadChange *= -1; }
            if (loadChange > this.previousCPULoad*0.1)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }

        public float GetAvailableRAM()
        {
            float currentAvailable = this.AvailableRAM;
            float availableChange = currentAvailable - this.previousRAMAvailable;
            if (availableChange < 0) { availableChange *= -1; } 
            if (availableChange > this.previousRAMAvailable*0.1)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentAvailable;
            return currentAvailable;
        }
    }
}
