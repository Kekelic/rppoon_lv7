﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    class LinearSearch : SearchStrategy
    {
        public bool Search(double[] array,double searchNumber)
        {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
                if (array[i] == searchNumber)
                    return true;
            return false;

        }
    }
}
