﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 4.1, 5, 2.45, 7.4, 9, 1.8, 3.4, -2.2, 3.3, 15.3, 0.9, 11.1 };
            NumberSequence numberSequence = new NumberSequence(array);

            numberSequence.SetSearchStrategy(new LinearSearch());
            double searchNumber = 11.1;
            if (numberSequence.Search(searchNumber))
                Console.WriteLine("Number "+searchNumber+" was found!");
            else
                Console.WriteLine("Number "+searchNumber+ " was not found!");

            searchNumber = 4.2;
            if (numberSequence.Search(searchNumber))
                Console.WriteLine("Number " + searchNumber + "was found!");
            else
                Console.WriteLine("Number " + searchNumber + " was not found!");

        }
    }
}
